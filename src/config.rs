use std::{fs::File, io::Read};
use serde_derive::Deserialize;
use std::collections::HashMap;
use once_cell::sync::OnceCell;

pub const CONFIG_FILE: &str = "config.toml";

#[derive(Debug, Deserialize)]
pub struct Config {
    pub base_url: String,
    pub base_index: String,
    pub monthly_reports: String,
    pub weekly_reports: String,
    pub month_names: [String; 12],
    pub services_names: HashMap<String, String>,
}

pub static INSTANCE: OnceCell<Config> = OnceCell::new();

impl Config {
    pub fn global() -> &'static Config {
        INSTANCE.get().expect("Config is not initialized")
    }

    fn read_config(filename: &str) -> Config {
        let mut config_file = File::open(filename).expect("Error: config.toml is missing.");
        let mut config_str = String::new();

        config_file
            .read_to_string(&mut config_str)
            .expect("Error: couldn't read config.toml to string.");

        toml::from_str(&config_str).expect("Error: couldn't parse the config.toml file.")
    }

    pub fn init() -> Config {
        Config::read_config(CONFIG_FILE)
    }
}
