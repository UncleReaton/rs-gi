// made for 42l stats

use std::{collections::HashMap, process::Command, str, io::Write, str::FromStr};
use sailfish::TemplateOnce;
use human_sort::sort;
use chrono::Local;

mod config;

fn print_type_of<T: ?Sized>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}

#[derive(TemplateOnce)]
#[template(path = "index.stpl")]
struct IndexTemplate<'a> {
    base_url: &'a str,
    years: &'a Vec<String>,
    months: &'a Vec<&'a str>,
    weeks: &'a Vec<&'a str>,
    sel_year: String,
}

#[derive(TemplateOnce)]
#[template(path = "index-service.stpl")]
struct IndexSrvTemplate<'a> {
    base_url: &'a str,
    services: &'a Vec<String>,
    sel_year: String,
    sel_period: String,
    folder_num: u8
}

fn main() {
    config::INSTANCE.set(config::Config::init()).unwrap();
    let g_instance = config::Config::global();
    println!("{}", g_instance.base_index);
    let current_year = Local::now().format("%Y").to_string();
    let years_list = list_files(format!("{}{}",g_instance.base_index, g_instance.monthly_reports));

    if !is_cur_year_log(current_year.as_str(), &years_list) {
        println!("Current year not logged yet. Happy New Year!");
        println!("Interrupting the program to avoid unpredictable behavior");
        std::process::exit(1);
    }

    let mut months_list = Vec::new();
    let mut weeks_list = Vec::new();

    for i in 0..years_list.len() {
        let selected_year = &years_list[i];
        months_list = list_files(format!("{}{}{}",g_instance.base_index, g_instance.monthly_reports, selected_year));
        weeks_list = list_files(format!("{}{}{}",g_instance.base_index, g_instance.weekly_reports, selected_year));

        // convert Vec<String> to Vec<&str>
        let mut months_list: Vec<_> = months_list.iter().map(String::as_str).collect();
        let mut weeks_list: Vec<_> = weeks_list.iter().map(String::as_str).collect();
        
        sort(&mut months_list);
        sort(&mut weeks_list);

        if !std::path::Path::new(format!("{}{}{}{}", g_instance.base_index, g_instance.monthly_reports, selected_year, "/index.html").as_str()).exists() {
            let ctx = IndexTemplate {
                base_url: &g_instance.base_url,
                years: &years_list,
                months: &months_list,
                weeks: &weeks_list,
                sel_year: selected_year.to_string(),
            };
            let mut file = std::fs::File::create(format!("{}{}{}{}", g_instance.base_index, g_instance.monthly_reports, selected_year, "/index.html").as_str()).unwrap();
            let write = writeln!(&mut file, "{}", ctx.render_once().expect("Failed to render_once monthly index"));
            match write {
                Ok(_) => (),
                Err(error) => println!("Whoops: {}", error),
            };
        }

        if !std::path::Path::new(format!("{}{}{}{}", g_instance.base_index, g_instance.weekly_reports, selected_year, "/index.html").as_str()).exists() {
            let ctx = IndexTemplate {
                base_url: &g_instance.base_url,
                years: &years_list,
                months: &months_list,
                weeks: &weeks_list,
                sel_year: selected_year.to_string(),
            };
            let mut file = std::fs::File::create(format!("{}{}{}{}", g_instance.base_index, g_instance.weekly_reports, selected_year, "/index.html").as_str()).unwrap();
            let write = writeln!(&mut file, "{}", ctx.render_once().unwrap());
            match write {
                Ok(_) => (),
                Err(error) => println!("Whoops: {}", error),
            };
        }

        for j in 0..weeks_list.len() {
            let week_num = u8::from_str(&weeks_list[j]).unwrap();
            let path = format!("{}{}{}/{}/", g_instance.base_index, g_instance.weekly_reports, years_list[i], week_num);
            let list_services = list_files(format!("{}all/", &path));
            if !std::path::Path::new(format!("{}{}", path, "index.html").as_str()).exists() {
                let ctx = IndexSrvTemplate {
                    base_url: &g_instance.base_url,
                    services: &list_services,
                    sel_year: selected_year.to_string(),
                    sel_period: "weekly".to_string(),
                    folder_num: week_num,
                };
                let mut file = std::fs::File::create(format!("{}{}", path, "index.html").as_str()).unwrap();
                let write = writeln!(&mut file, "{}", ctx.render_once().unwrap());
                match write {
                    Ok(_) => (),
                    Err(error) => println!("Whoops: {}", error),
                };
            }
        }

        for j in 0..months_list.len() {
            let month_num = u8::from_str(&months_list[j]).unwrap();
            let path = format!("{}{}{}/{}/", g_instance.base_index, g_instance.monthly_reports, years_list[i], month_num);
            let list_services = list_files(format!("{}all/", &path));
            if !std::path::Path::new(format!("{}{}", path, "index.html").as_str()).exists() {
                let ctx = IndexSrvTemplate {
                    base_url: &g_instance.base_url,
                    services: &list_services,
                    sel_year: selected_year.to_string(),
                    sel_period: "monthly".to_string(),
                    folder_num: month_num,
                };
                let mut file = std::fs::File::create(format!("{}{}", path, "index.html").as_str()).unwrap();
                let write = writeln!(&mut file, "{}", ctx.render_once().unwrap());
                match write {
                    Ok(_) => (),
                    Err(error) => println!("Whoops: {}", error),
                };
            }
        }
    }

    // Rendering main Index
    if !std::path::Path::new(format!("{}{}", g_instance.base_index, "/index.html").as_str()).exists() {
        let ctx = IndexTemplate {
            base_url: &g_instance.base_url,
            years: &years_list,
            months: &months_list.iter().map(String::as_str).collect(),
            weeks: &weeks_list.iter().map(String::as_str).collect(),
            sel_year: current_year,
        };
        let mut file = std::fs::File::create(format!("{}{}", g_instance.base_index, "/index.html").as_str()).unwrap();
        let write = writeln!(&mut file, "{}", ctx.render_once().unwrap());
        match write {
            Ok(_) => (),
            Err(error) => println!("Whoops: {}", error),
        };
    }
}

// format month number (here value) is convert to his proper name
fn month_format(value: usize) -> &'static str {
    let g_instance = config::Config::global();
    &g_instance.month_names[value - 1] 
}

// Check if service exists using its file name without the extension
fn srv_format<'a>(file: &'a str, services: HashMap<&str, &'a str>) -> &'a str {
    let value = file.split('.');
    let split: Vec<&str> = value.collect();
    let service_formated = split.first().unwrap();
    println!("{:?}", service_formated);
    for (service, name) in services {
        if service_formated == &service {
            return name;
        }
    }
    println!("No service name specified for file {}", file);
    file
}

// shell's command 'ls' implementation
fn list_files(folder: String) -> Vec<String> {
    let command = Command::new("ls")
        .arg(folder)
        .output()
        .expect("Failed to execute the command :( ");

    let output_string = str::from_utf8(command.stdout.as_slice())
                        .unwrap()
                        .split('\n');
    let mut output_vec = Vec::new();
    for string in output_string {
        output_vec.push(string.to_string());
    }
    output_vec.pop();
    if output_vec.last().unwrap() == "index.html" {
        output_vec.pop();
    }
    output_vec
}
/*
fn convert_ls_type(list: Vec<String>) -> Vec<&'static str> {
    let mut new_list: Vec<&str> = Vec::new();
    let list = list.clone();
    
    for s in list.iter() {
        new_list.push(s);
    }
    new_list
}*/

// Check if currrent year (cur_year) is already logged in the years_list
fn is_cur_year_log(cur_year: &str, years_list: &[String]) -> bool {
    for years in years_list {
        if years == cur_year {
            return true;
        }
    } 
    false
}
