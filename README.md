# RS-GI  
## Rust implementation of [gen-indexes](https://git.42l.fr/42l/gen-indexes)  
It aims to have better performances than gen-indexes.  
### [License](LICENSE)
This project is under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.txt) license.
